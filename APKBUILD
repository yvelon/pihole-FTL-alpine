# Maintainer: yvelon <yvelon@yandex.com>
pkgname=pihole-FTL
_pkgname=FTL
pkgver=5.25.1
pkgrel=1
pkgdesc="FTLDNS™ (pihole-FTL) provides an interactive API and also generates statistics for Pi-hole®'s Web interface."
url="https://github.com/pi-hole/FTL"
arch="all"
license="EUPL-1.2"
makedepends="cmake libidn-dev gmp-dev nettle-dev readline-dev linux-headers libcap"
source="$_pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz dynamic-link.patch message-table.patch network-table.patch"
options="!check setcap"
builddir="$srcdir/$_pkgname-$pkgver"

export GIT_VERSION=$pkgver
export GIT_TAG=$pkgver

prepare() {
	cd "$builddir"
	patch -Np1 -i "$srcdir/dynamic-link.patch"
	if uname -m | grep -q armv7l; then
	        patch -Np1 -i "$srcdir/message-table.patch"
	        patch -Np1 -i "$srcdir/network-table.patch"
	fi
	if uname -m | grep -q i686; then
	        patch -Np1 -i "$srcdir/message-table.patch"
	        patch -Np1 -i "$srcdir/network-table.patch"
	fi
	echo '#define ASSERT_SIZEOF(OBJECT, SIZE64, SIZE32, SIZEARM)' > src/static_assert.h
	mkdir build
}

build() {
	cd "$builddir/build"
	cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_C_FLAGS="$CFLAGS -Wno-enum-int-mismatch -Wno-use-after-free"
	# Remove warnings as errors, should find a better way to do so rather than hacking the cmake files
	find src/ -name "*.make" -exec sed -i 's/-Werror / /g' {} \;
	make
}

package() {
	cd "$builddir/build"
	make DESTDIR="$pkgdir" install
}


sha512sums="ee11ae8ced40977de550f2c397b27e65f553310436eaa32b62fb5ad60ec944814ba716b8aae526b815fbfc854198ec334e20f21c1c4130827e590c3b62ebc44d  FTL-5.25.1.tar.gz
ca1c9454f41d07e28d8c098e2196f00219b0b6f99466ade8f47a4bfd8f77a6df4592b411d8b28361e0457b369f7d19f2b0cef9c2b3b6d79041606cabf0990230  dynamic-link.patch
bc4ee2dd623ee4ae0fcad1bd0c98a230dfcc03d56ddf2d90cab9e68ce0b681de927627561e0c5d03822ae5279007f7e75b27c94715f583b68eb479ad0626bab7  message-table.patch
260c95492476d181a77efb0b44253e5ddce3d9626435ee950fd42d582f4c50590368b1e10620e4820f05c15f34b1a38dc5828be3d20747290030c7d8f77d6f67  network-table.patch"

